const rp = require('request-promise')
const cheerio = require('cheerio')
 
const options = {
  uri: 'https://www.petlove.com.br/cachorro',
  transform: function (body) {
    return cheerio.load(body)
  }
}
 
function processarDados(dados){
  //salva no banco de dados
  console.log(JSON.stringify(dados))
}
 
rp(options)
.then(($) => {
  const products = []
  $('.catalog-list-item').each((i, item) => {
    const product = {
      description: $(item).find('.product-name').text(),
      picture: $(item).find('img').attr("src")
    }
 
    if(product.nome !== "")
      products.push(product)
  })
  processarDados(products)
})
.catch((err) => {
  console.log(err);
})